# UDP_Connect

This project gives example connection info for the UDP (both context store and event store) using RStudio

## RStudio Access
Each university has an instance of RStudio, and authentication is granted using SSO while on the university VPN. Also, certain research projects have their own instances of RStudio made specifically for that work. 

If you have trouble accessing RStudio or would like to explore creating an instance, please contact Kyle Unruh (kyle.unruh@unizin.org)


## Context Store
The script "context_store.R" shows how to connect to the context store. Comments are given where institution-specific or project-specific information needs to be replaced.

A config.yml file needs to be included for the DB connection. This varies based on the institution and project, so rely on the connection information given by the Unizin Services team. The format of the config.yml file should be the following:

```YAML
default:
  context_store:
    driver: 'Postgres'
    hostname: '<DB HOST NAME>'
    username: '<DB USERNAME>'
    password: '<DB PASSWORD>'
    port: 5432
    database: 'context_store'
```

Upload this config.yml file into the same directory as your R script.

Note: If you are using version control for your work, please ensure that the config.yml file is included in your .gitignore file so that sensitive credentials are not shared widely.

## Event Store
The script "event_store.R" shows how to connect to the event store. Comments are given where institution-specific or project-specific information needs to be replaced.

A GCP service account is required to authenticate against the event store in Google BigQuery. This is a JSON file that Unizin can generate for you from the GCP console. Similar to the config.yml file connecting to the context store, this JSON service account needs to be uploaded into R Studio. Also, make sure this file is included in a .gitignore file to prevent widely sharing.


