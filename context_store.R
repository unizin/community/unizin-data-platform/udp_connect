# Connecting to the UDP context store

#Include these libraries
#tidyverse is also useful for analysis but not required
library(DBI)
library(RPostgreSQL)
library(config)
library(tidyverse)

#this is config.yml; upload to the same location as your script
dw <- config::get("context_store") 

#create the connection
con <- DBI::dbConnect(RPostgreSQL::PostgreSQL(),
                      dbname   = dw$database,
                      user     = dw$username,
                      password = dw$password,
                      host     = dw$hostname,
                      port     = dw$port)


### Method 1: Using SQL string and DBI
#SQL Query string
#Feel free to define this string as you like!
query = 'SELECT * FROM entity.academic_term'

#Query UDP and store results in an R dataframe
method1_academic_term <- dbGetQuery(con, query)


### Method 2: Using tidyverse and dbplyr
method2_academic_term <- tbl(con, in_schema("entity", "acadademic_term"))
view(course_offering)
